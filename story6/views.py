from django.shortcuts import render
from .forms import Forms
from story6 import forms
from django.urls import reverse
from django.http import HttpResponseRedirect
from story6.models import Status


# Create your views here.


def index(request):
    response = {
        'status': Forms,
        'events': Status.objects.all()
    }
    return render(request, 'status.html', response)


def add_status(request):
    forms = Forms(request.POST)
    if request.method == "POST":
        if forms.is_valid():
            clean = forms.cleaned_data
            status = Status()
            status.forms = clean['status']
            status.save()
            print(status)
            return HttpResponseRedirect('/')
    else:
        return request, 'status.html'
