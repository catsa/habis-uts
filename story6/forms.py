from django import forms


class Forms(forms.Form):
    status_attrs = {
        "type": "text",
        "placeholder": "Hello, how are you ?",
        "class": "search-text",
        "name": "newstatus"
    }
    status = forms.CharField(
        max_length=200, required=True, widget=forms.TextInput(attrs=status_attrs))
