# Generated by Django 2.2.5 on 2019-11-03 07:07

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('forms', models.CharField(max_length=200)),
            ],
        ),
    ]
