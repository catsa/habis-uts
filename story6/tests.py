from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from story6.views import index
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import time
import re
from datetime import timezone
from story6.models import Status
from selenium.webdriver.chrome.options import Options


# Create your tests here.


class StatusTest(TestCase):
    def test_landing_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_landing_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'status.html')

    def test_landing_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


class SeleniumTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(SeleniumTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SeleniumTest, self).tearDown()

    def test_automated_if_add_status_available(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        time.sleep(2)

        masukan = selenium.find_element_by_name('status')

        # Fill the form with data
        masukan.send_keys('coba coba')

        # submitting the form
        masukan.send_keys(Keys.RETURN)

        # testing whether it contains coba coba using regex
        src = selenium.page_source
        text_found = re.search(r'coba coba', src)
        self.assertNotEqual(text_found, None)
