from django.db import models

# Create your models here.


class Status(models.Model):
    id = models.AutoField(primary_key=True)
    forms = models.CharField(max_length=200)
