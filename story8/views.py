from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.


def index(request):
    return render(request, "story8.html")


def populateToSearch(request, find="minecraft"):
    link = "https://www.googleapis.com/books/v1/volumes?q=" + find
    data_final = request.get(link).json()
    return JsonResponse(data_final)
