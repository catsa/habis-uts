from django.test import TestCase
from django.urls import resolve
from story9.views import index, register

# Create your tests here.


class Story9Test(TestCase):
    def test_home_page_exists(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = self.client.get('/story9/')
        self.assertTemplateUsed(response, 'story9.html')

    def test_template_register(self):
        response = self.client.get('/story9/register/')
        self.assertTemplateUsed(response, 'registration/register.html')

    def test_template_login(self):
        response = self.client.get('/story9/login/')
        self.assertTemplateUsed(response, 'registration/login.html')
