from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.contrib.auth import authenticate, logout
from story9.forms import RegisterForm

# Create your views here.


def index(request):
    return render(request, "story9.html")


@login_required
def greetings(request):
    return render(request, "greetings.html")


def register(response):
    if response.method == "POST":
        form = RegisterForm(response.POST)
        if form.is_valid():
            form.save()
            new_user = form.save()
            new_user = authenticate(
                username=response.POST['username'], password=response.POST['password1'])
            return redirect('/story9/')
    else:
        form = RegisterForm()
    return render(response, "registration/register.html", {'form': form})
