from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *

class Lab7UnitTests(TestCase):
    def test_url_exists(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)
        invalid_response = Client().get('/this_should_not_exist')
        self.assertEqual(invalid_response.status_code, 404)
    
    def test_landing_using_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_story7_using_index_function(self):
        foundFunc = resolve('/story7/')
        self.assertEqual(foundFunc.func, profile)
